function run_cpmc_dataset(exp_dir, imgsetpath, dataset, mask_type, pb_types, overwrite)
    DefaultVal('overwrite',false);
    [directories, img_names] = parse_dataset(imgsetpath, dataset);
% Compute all flows
    compute_all_flow(exp_dir, directories, img_names, overwrite);
% Compute all gb
    compute_all_boundaries(exp_dir, directories, img_names, true, overwrite);
    if ~exist('mask_type','var') || isempty(mask_type)
        mask_type = 'WithOpticalFlow';
    end
    for i=1:length(img_names)
% CPMC        
%        cpmc_maskonly(exp_dir,directories{i}, img_names{i},segm_pars, overwrite);
% FastSeg
    masks = false(0);
    img_dir = [exp_dir 'JPEGImages/' directories{i} '/'];
    file_ext = get_img_extension(img_dir, img_names{i});
    filename = [img_dir img_names{i} file_ext];
    if ~exist('pb_folder','var') || isempty(pb_folder)
        pb_types = {'Gb','Gb_flow','Gb_plusflow'};
    end
    for j=1:length(pb_types)
        file_params.data_save_dirpath = exp_dir;
        segm_params.boundaries_method = pb_types{j};
        segm_params.pmc_maxflow_method = 'hochbaum';
        segm_params.graph_methods = {'UniformGraphFuxin','ColorGraphFuxin'};
        segm_params.graph_sub_methods = {{'internal','external','external2'},{'internal','external','external2'}};
        segm_params.graph_sub_methods_seeds_idx = {[1,1,1],[2,2,2]};
        segm_params.graph_sol_upper_bp = [20,300];
     %   segm_params.graph_seed_gen_method = {'sp_seed_sampling', 'sp_clr_seeds'};
    %    segm_params.graph_seed_params = {{64, 4, 'trained_models/train_trees.mat',4},{[8 8], [15, 15]}};
        segm_params.graph_seed_params = { {[5 5], [40, 40]},     {[5 5], [15, 15]}    };
        segm_params.graph_pairwise_sigma = {1,1.5};
        other_params.force_recompute = overwrite;

        this_masks = fast_obj_segments(filename,'filepath_params',file_params,'segm_params',segm_params,'other_params',other_params);

        % Remove the file, otherwise it would err on the next boundaries
        delete([exp_dir 'MySegmentsMat/' directories{i} '/' img_names{i} '.mat']); 
        masks = cat(3, masks, this_masks);
        if j > 1
            masks = remove_repeated_segments(masks);
        end
    end
    the_save_dir = [exp_dir 'MySegmentsMat/' mask_type '/' directories{i} '/'];
    if ~exist(the_save_dir, 'dir')
        mkdir(the_save_dir);
    end
    save([exp_dir 'MySegmentsMat/' mask_type '/' directories{i} '/' img_names{i} '.mat'], 'masks');
    end
end

function segments = remove_repeated_segments(segments)
    OVERLAP_THRESH = 0.95;

    [size1, size2, size3] = size(segments);
    segments = reshape(segments, size1 * size2, size3);
    overlap_mat = overlap_over_threshold(segments, ...
                                         OVERLAP_THRESH);
    bw_mat = overlap_mat >= OVERLAP_THRESH;
    [num_conn, conncomps] = graphconncomp(sparse(bw_mat));
    this_sel = zeros(1,num_conn);
    for i = 1:num_conn
        s1 = find(conncomps == i);
        % Avoid computing energies, just use a random one
        b = randperm(length(s1),1);
        this_sel(i) = s1(b);
    end
    segments = segments(:, this_sel);
    segments = reshape(segments, size1, size2, num_conn);
end
