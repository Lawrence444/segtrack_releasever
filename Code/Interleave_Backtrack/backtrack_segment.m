function [LinReg_obj,w] = backtrack_segment(ConfigFile_or_opts, LinReg_objs_all, rf_obj,w, directories, img_names, track_ids, inference_type, mask_type)
    if ~isstruct(ConfigFile_or_opts)
        eval(ConfigFile_or_opts);
    else
        SVMSEGMopts = ConfigFile_or_opts;
    end
    DefaultVal({'inference_type','mask_type','reg_param','restart_frames'},{'simple',SVMSEGMopts.mask_type,2000,5});

    masks_cur = load([SVMSEGMopts.segment_matrice_dir mask_type '/' directories{length(img_names)} '/' img_names{length(img_names)} '.mat']);
    masks_cur = masks_cur.masks(:,:,track_ids{length(img_names)});
    motion_feat_cur = zeros(size(masks_cur,3),2);
    [motion_feat_cur(:,1), motion_feat_cur(:,2)] = compute_centroid(masks_cur);
    for i=length(img_names)-1:-1:1
        masks_prev = load([SVMSEGMopts.segment_matrice_dir mask_type '/' directories{i} '/' img_names{i} '.mat']);
        masks_prev = masks_prev.masks;
        motion_feat_prev = zeros(size(masks_prev,3),2);
        [motion_feat_prev(:,1), motion_feat_prev(:,2)] = compute_centroid(masks_prev);

        overlay_mat = false(size(masks_prev,3),size(masks_cur,3));
        for j=1:size(masks_cur,3)
            % Find the segments that stay within motion constraints
            overlay_mat(:,j) = sum(abs(bsxfun(@minus, motion_feat_cur(j,:), motion_feat_prev))>0.15,2) == 0;
        end

        t = tic();
        disp('Overlap time: ');
        toc(t);
        %        end
        % Load input features
        features_prev = get_features(SVMSEGMopts.tracking_features, SVMSEGMopts.measurement_dir, ...
            SVMSEGMopts.scaling_types, mask_type, directories{i}, img_names{i});

        XX1 = rf_featurize(rf_obj, features_prev{1}');
        XX2 = rf_featurize(rf_obj, features_prev{2}');
        allX_test = [XX1 XX2];
        if LinReg_objs_all{i+1}.no_target()
            masks_cur = masks_prev(:,:,track_ids{i});
            motion_feat_cur = motion_feat_prev(track_ids{i},:);
            continue;
        end
        pred = bsxfun(@plus, w{i+1}(1,:), allX_test * w{i+1}(2:end,:));
        if strcmp(inference_type, 'simple')
                if ~isempty(pred)
                    [max_pred_score,backward_assignment] = max(pred .* overlay_mat);
                else
                    a = single([]);
                end
                backward_assignment = backward_assignment';
            %         elseif strcmp(inference_type,'hungarian')
            %             % Still use overlay
            %             for j=1:length(pred)
            %                 if ~isempty(new_trackid{j})
            %                     pred{j} = pred{j} .* overlay_mat(:,new_trackid{j});
            %                 else
            %                     pred{j} = single([]);
            %                 end
            %             end
            %             pred_all = 1 -cell2mat(pred');
            %             % Just increase the ones that we don't want to match
            %             pred_all(pred_all >= 0.8) = inf;
            %             pred_all(pred_all < 0) = 0;
            %             [forward_assignment, cost] = assignmentoptimal(double(pred_all)');
            %             % backward_assignment can be done by this because Hungarian
            %             % matching will automatically be unique, other than 0 (no
            %             % match)
            %             [unique_segs,backward_assignment] = unique(forward_assignment);
            %             unique_segs = unique_segs';
            %             if unique_segs(1) == 0
            %                 unique_segs = unique_segs(2:end);
            %                 backward_assignment = backward_assignment(2:end);
            %             end
            %             inds = sub2ind(size(pred_all), unique_segs, mat_trackid(backward_assignment));
            %            scores = 1 - pred_all(inds);
        end
        % Otherwise trim the tracks
        %            features_cur = cellfun(@(x) x(:,unique_segs), features_next,'UniformOutput',false);
        %            masks_cur = masks_next(:,:,unique_segs);
        masks_cur = masks_prev(:,:,[track_ids{i};backward_assignment]);
        LinReg_obj_cur = LinReg_objs_all{i};
        % Same frame, just want to get it for the output. 
        overl_mat = fast_segm_overlap_mex(masks_prev, masks_prev(:,:,backward_assignment));
        LinReg_obj_cur.renew_targets([LinReg_obj_cur.InputTarget LinReg_objs_all{i+1}.InputTarget+bsxfun(@times, max_pred_score,[ones(size(allX_test,1),1) allX_test]' * overl_mat)]);
        w{i} = LinReg_obj_cur.Regress(reg_param);
        %            motion_feat_cur = motion_feat_next(unique_segs,:);
        motion_feat_cur = motion_feat_prev([track_ids{i};backward_assignment],:);
    end
    LinReg_obj = LinReg_obj_cur;
    w = w{1};
end
