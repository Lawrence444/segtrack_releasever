function w = find_specific_track_by_matching(w_save, new_trackids, want_tracks)
% Use new_trackids returned by plot_long_tracks_from_back_map and w_save
% from track_sequence_multiple_LinReg_interleave_backtrack to locate the
% weight vectors for the specific tracks user wanted
% w_save: output of track_sequence_multiple_LinReg_interleave_backtrack
% new_trackids: output of plot_long_tracks_from_back_map
% want_tracks: the id of the tracks you want

counter = 0;
end_pos = [w_save.end_frame];
for i=length(new_trackids):-1:1
    track_this = find(want_tracks > counter & want_tracks <= counter + numel(new_trackids{i}));
    w_loc = find(end_pos == i+1);
    for j=1:length(w_loc)
        [C, ia, ib] = intersect(w_save(w_loc(j)).last_id, new_trackids{i}(want_tracks(track_this) - counter));
        w(:,track_this(ib)) = w_save(w_loc(j)).w(:,ia);
    end
    counter = counter + numel(new_trackids{i});
end