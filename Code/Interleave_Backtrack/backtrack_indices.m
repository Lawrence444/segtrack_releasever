function track_ids = backtrack_indices(track_ids, back_map)
    for i=size(back_map,2):-1:1
        for j=1:min(length(track_ids),i)
            track_ids{j} = back_map(track_ids{j},i);
        end
    end
end
