function [forward_map, back_map, LinReg_obj, rf_obj, w_saves] = track_sequence_multiple_LinReg_interleave_backtrack(ConfigFile_or_opts, directories, img_names, inference_type, mask_type, Napp, reg_param, beat_length, save_w_length)
% Minimal call is: track_sequence_multiple_LinReg_w_backtrack_every_possible_loc(ConfigFile_or_opts, directories, img_names)
if ~isstruct(ConfigFile_or_opts)
    eval(ConfigFile_or_opts);
else
    SVMSEGMopts = ConfigFile_or_opts;
end
DefaultVal({'inference_type','mask_type','Napp','reg_param','restart_frames','beat_length'},{'simple',SVMSEGMopts.mask_type,3000,10000,5,5});

options.method = 'direct';
options.Nperdim = 5;
options.single = true;
features_cur = get_features(SVMSEGMopts.tracking_features, SVMSEGMopts.measurement_dir, ...
    SVMSEGMopts.scaling_types, mask_type, directories{1}, img_names{1});
options.params = decide_parameter_list(cell2mat(features_cur),20);
rf_obj = InitExplicitKernel('exp_chi2',1.5,300,Napp,options);
masks_cur = load([SVMSEGMopts.segment_matrice_dir mask_type '/' directories{1} '/' img_names{1} '.mat']);
masks_cur = masks_cur.masks;
new_trackid = cell(1,1);
new_trackid{1} = 1:size(masks_cur,3);
velocity_model = cell(1,1);
going_out = cell(1,1);
smoothness = cell(1,1);
LinReg_obj = [];
motion_feat_cur = zeros(size(masks_cur,3),2);
[motion_feat_cur(:,1), motion_feat_cur(:,2)] = compute_centroid(masks_cur);
old_unique_segs = 1:size(masks_cur,3);
beats_start = 1:beat_length:(length(img_names)-1);
beats_end = beat_length:beat_length:(length(img_names) - 1);
if length(beats_end) < length(beats_start)
    beats_end = [beats_end, length(img_names) - 1];
end
weights = ones(size(masks_cur,3),1);
len_tracks = 0;
beats_counter = 1;
save_counter = 1;
LinReg_start = 1;
for i=1:length(img_names)-1
    i
    new_trackid
    if directories{i} ~= directories{i+1}
        disp('Currently does not support simultaneous tracking multiple sequences yet! Will add later.');
    end
    
    masks_next = load([SVMSEGMopts.segment_matrice_dir mask_type '/' directories{i} '/' img_names{i+1} '.mat']);
    masks_next = masks_next.masks;
    motion_feat_next = zeros(size(masks_next,3),2);
    [motion_feat_next(:,1), motion_feat_next(:,2)] = compute_centroid(masks_next);
    
    overlay_mat = false(size(masks_next,3),length(old_unique_segs));
    for j=1:length(old_unique_segs)
        % Find the segments that stay within motion constraints
        segsize = sum(sum(masks_cur(:,:,old_unique_segs(j))));
        imgsize = size(masks_cur(:,:,old_unique_segs(j)), 1) * size(masks_cur(:,:,old_unique_segs(j)), 2);
        
        % restrict the movement range of the segment
        if(segsize < imgsize * 0.05)
            ratio = segsize / imgsize;
            overlay_mat(:,j) = sum(abs(bsxfun(@minus, motion_feat_cur(old_unique_segs(j),:), motion_feat_next))>(ratio * 3),2) == 0;
        else
            overlay_mat(:,j) = sum(abs(bsxfun(@minus, motion_feat_cur(old_unique_segs(j),:), motion_feat_next))>0.15,2) == 0;
        end
    end
    % Overl_mat will be the output
    
    t = tic();
    overl_mat = fast_segm_overlap_mex(masks_cur(1:2:end,1:2:end,:), masks_cur(1:2:end,1:2:end,old_unique_segs));
    disp('Overlap time: ');
    toc(t);
    %        end
    % Load input features
    features_next = get_features(SVMSEGMopts.tracking_features, SVMSEGMopts.measurement_dir, ...
        SVMSEGMopts.scaling_types, mask_type, directories{i+1}, img_names{i+1});
    t = tic();
    [pred, w, LinReg_obj] = do_traintest_1frame(features_cur, features_next, overl_mat, reg_param, rf_obj, LinReg_obj, new_trackid, weights);
    disp('Train_test time: ');
    toc(t);
    
    ranges = [0 cumsum(cellfun(@length,new_trackid))];
    mat_trackid = cell2mat(new_trackid);
    if strcmp(inference_type, 'simple')
        a = cell(1,length(LinReg_obj));
        b = cell(1,length(LinReg_obj));
        weights = zeros(size(masks_next,3),length(LinReg_obj));
        for j=1:length(LinReg_obj)
            if ~isempty(pred{j})
                [a{j},b{j}] = max(pred{j} .* overlay_mat(:,new_trackid{j}),[],1);
                weights(:,j) = max(pred{j} .* overlay_mat(:,new_trackid{j}),[],2);
                % Compensate a little bit so longer tracks are easier to be
                % matched
                % When we have len_tracks, we have going_out also
                if len_tracks(j) > 0
                    % Compensate long tracks only when it's not going out
                    % of the screen
                    a{j}(going_out{j}==0) = a{j}(going_out{j}==0) + len_tracks(j) * 0.01 * exp(-4 * smoothness{j}(going_out{j}==0))';
                    % When it is going out, make the score much lower
                    a{j}(going_out{j}~=0) = a{j}(going_out{j}~=0) - 0.1 * going_out{j}(going_out{j} ~= 0)';
                end
            else
                a{j} = single([]);
            end
        end
        % Everything together
        a = cell2mat(a);
        forward_assignment = cell2mat(b);
        [c,d] = sort(a,'descend');
        % Cut ones with really low scores
        cut_plc = find(c>=0.1,1,'last');
        [unique_segs,m] = unique(forward_assignment(d(1:cut_plc)),'first');
        backward_assignment = d(m);
    elseif strcmp(inference_type,'hungarian')
        weights = zeros(size(masks_next,3),length(LinReg_obj));
        % Still use overlay
        for j=1:length(pred)
            if ~isempty(new_trackid{j})
                pred{j} = pred{j} .* overlay_mat(:,new_trackid{j});
                weights(:,j) = max(pred{j} .* overlay_mat(:,new_trackid{j}),[],2);
            else
                pred{j} = single([]);
            end
        end
        pred_all = 1 -cell2mat(pred');
        % Just increase the ones that we don't want to match
        pred_all(pred_all >= 0.8) = inf;
        pred_all(pred_all < 0) = 0;
        [forward_assignment, cost] = assignmentoptimal(double(pred_all)');
        % backward_assignment can be done by this because Hungarian
        % matching will automatically be unique, other than 0 (no
        % match)
        [unique_segs,backward_assignment] = unique(forward_assignment);
        unique_segs = unique_segs';
        if unique_segs(1) == 0
            unique_segs = unique_segs(2:end);
            backward_assignment = backward_assignment(2:end);
        end
        inds = sub2ind(size(pred_all), unique_segs, mat_trackid(backward_assignment));
        %            scores = 1 - pred_all(inds);
    end
    forward_map(old_unique_segs(mat_trackid),i) = forward_assignment;
    back_map(unique_segs,i) = old_unique_segs(mat_trackid(backward_assignment));
    %        back_scores(unique_segs,i) = scores;
    for j=1:length(LinReg_obj)
        dm_inrange = backward_assignment > ranges(j) & backward_assignment <= ranges(j+1);
        tracks_tokeep = backward_assignment - ranges(j);
        tracks_tokeep = tracks_tokeep(dm_inrange);
        if exist('save_w_length','var') && i - LinReg_start(j) + 1 >= save_w_length
            thrown_tracks = ~ismember(1:size(w{j},2), tracks_tokeep);
            w_saves(save_counter).w = w{j}(:,thrown_tracks);
            w_saves(save_counter).last_id = new_trackid{j}(thrown_tracks);
            w_saves(save_counter).start_frame = LinReg_start(j);
            w_saves(save_counter).end_frame = i;
            save_counter = save_counter + 1;
        end
        new_trackid{j} = unique_segs(dm_inrange);
        LinReg_obj{j}.prune_targets(tracks_tokeep);
        w{j} = w{j}(:,tracks_tokeep);
        if ~isempty(velocity_model{j})
            [velocity_model{j}, going_out_this, cur_smoothness] = determine_velocity(motion_feat_cur(back_map(unique_segs(dm_inrange),i),1:2), ...
                             motion_feat_next(unique_segs(dm_inrange),1:2), velocity_model{j}(tracks_tokeep,:), len_tracks(j)-1);

            smoothness{j} = (smoothness{j}(tracks_tokeep,:) * (len_tracks(j) -1) + cur_smoothness) / len_tracks(j);
            if ~isempty(tracks_tokeep)
                going_out{j} = going_out{j}(tracks_tokeep) .* going_out_this + going_out_this;
            else
                going_out{j} = zeros(0,1);
            end

        else
        [velocity_model{j}, going_out{j}, smoothness{j}] = determine_velocity(motion_feat_cur(back_map(unique_segs(dm_inrange),i),1:2), ...
                             motion_feat_next(unique_segs(dm_inrange),1:2));
        end
        % if going_out_this = 0, it's no longer going out. If it's 1, then
        % add the number with the number of previous frames that are going
        % out.

    end
    len_tracks = len_tracks + 1;
    
    a = sort(weights,'descend');
    inds = cellfun(@numel,new_trackid);
    objs = 1:length(LinReg_obj);
    objs = objs(inds~=0);
    inds = inds(inds~=0);
    inds = sub2ind(size(a),inds, objs);
    weights_cutoff = zeros(1,size(weights,2));
    weights_cutoff(inds~=0) = a(inds);
    weights_cutoff = max(weights_cutoff,0);
    weights = bsxfun(@rdivide, weights, weights_cutoff + eps);
    weights(weights>1) = 1;
    weights(weights<0) = 0;
    % Now, always find the "leftovers"
    leftovers = setdiff(1:size(masks_next,3),unique_segs);
    if ~isempty(leftovers) && i <= length(img_names) - beat_length
        % Then start a new LinReg_obj on those leftover segments
        LinReg_obj{j+1} = [];
        new_trackid{j+1} = leftovers;
        velocity_model{j+1} = [];
        going_out{j+1} = [];
        smoothness{j+1} = [];
        LinReg_start = [LinReg_start i+1];
    end
    old_unique_segs = 1:size(masks_next,3);
    len_tracks = [len_tracks;0];
    weights = [weights ones(size(masks_next,3), 1)];
    features_cur = features_next;
    masks_cur = masks_next;
    motion_feat_cur = motion_feat_next;
    % Find the LinReg_obj that needs to be backtracked to the start of the
    % beat, for each time, at most one LinReg_obj needs to be backtracked
    if i == beats_end(beats_counter)
        if beats_counter > 1 && i ~= length(img_names)-1
        % Always backtrack the last beat (not the current one!)
            i0 = beats_start(beats_counter -1);
            i1 = beats_end(beats_counter - 1);
            % Leftover adds one...
            id_start = length(LinReg_obj) - beat_length * 2;
            id_end = length(LinReg_obj) - beat_length - 1;
            % Find the indices for each track, at the end of the last beat
            new_trackid_at5 = backtrack_indices(new_trackid(id_start:id_end), back_map(:,i0:end));
            [Linr,w] = backtrack_segment(SVMSEGMopts, LinReg_obj(id_start:id_end), rf_obj, w(id_start:id_end), directories(i0:i1),...
                img_names(i0:i1), new_trackid_at5);
            LinReg_obj{id_start} = Linr;
            for j=id_start+1:id_end
                LinReg_obj{j} = [];
            end
            LinReg_obj = LinReg_obj([1:id_start (id_end+1):end]);
            new_trackid{id_start} = cell2mat(new_trackid(id_start:id_end));
            velocity_model{id_start} = cell2mat(velocity_model(id_start:id_end)');
            going_out{id_start} = cell2mat(going_out(id_start:id_end)');
            smoothness{id_start} = cell2mat(smoothness(id_start:id_end)');
            new_trackid = new_trackid([1:id_start (id_end+1):end]);
            velocity_model = velocity_model([1:id_start (id_end+1):end]);
            going_out = going_out([1:id_start (id_end+1):end]);
            smoothness = smoothness([1:id_start (id_end+1):end]);
            LinReg_start = LinReg_start([1:id_start (id_end+1):end]);
%             
%             % Re-assign old_unique_segs and new_trackid
%             old_unique_segs = cell2mat(new_trackid);
%             counter = 0;
%             for j=1:length(new_trackid)
%                 new_trackid{j} = counter+1:counter+length(new_trackid{j});
%                 counter = counter + length(new_trackid{j});
%             end
            % Update weights
            weights(:,id_start) = max(weights(:,id_start:id_end),[],2);
            weights = weights(:,[1:id_start (id_end+1):end]);
            len_tracks = len_tracks([1:id_start (id_end+1):end]);
        end
        beats_counter = beats_counter + 1;
    end
end
% Appending zeros to back_map to avoid overflow
if size(back_map,1) < size(forward_map,1)
    back_map = [back_map;zeros(size(forward_map,1) - size(back_map,1),size(back_map,2))];
end
for j=1:length(LinReg_obj)
    if exist('save_w_length','var') && i - LinReg_start(j) + 2 >= save_w_length
        w_saves(save_counter).w = w{j};
        w_saves(save_counter).last_id = new_trackid{j};
        w_saves(save_counter).start_frame = LinReg_start(j);
        w_saves(save_counter).end_frame = i + 1;
        save_counter = save_counter + 1;
    end
end
end
