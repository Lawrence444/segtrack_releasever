function [velocity_model, going_out, smoothness] = determine_velocity(centroid_cur, centroid_next, old_velocity, old_length)
% Suppose centroids have been normalized to [0,1]
    if ~exist('old_velocity','var') || isempty(old_velocity)
        velocity_model = centroid_next - centroid_cur;
	smoothness = zeros(size(velocity_model,1),1);
    else
        % Use a maximum of 5 frames for constant velocity model
        if old_length > 4
            old_length = 4;
        end
        % Average of those frames
        velocity_model = ((centroid_next - centroid_cur) + old_length * old_velocity) / (old_length+1);
        smoothness = sum(abs(velocity_model - old_velocity),2);
    end
    plc_next = centroid_next + velocity_model;
    going_out = double(plc_next(:,1) > 1 | plc_next(:,1) < 0 | plc_next(:,2) > 1 | plc_next(:,2) < 0);
end