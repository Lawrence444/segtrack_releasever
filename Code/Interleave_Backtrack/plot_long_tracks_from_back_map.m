function [qual_track, num_tracks, new_trackids] = plot_long_tracks_from_back_map(exp_dir, directories, img_names, mask_type, back_map, min_length, merge_plots)
% The returned new_trackids contains ordered tracks, where the first track
% is the first one that ends in the last frame (new_trackids{end}), then
% all the tracks ending in the last frame, then all the tracks ending in
% the second-to-last frame, and so on.
% Therefore, given a track number, one can track it back using new_trackids
    DefaultVal({'min_length','no_plot','merge_plots'},{7,false,[]});   
    back_tracker = find(back_map(:,end)~=0);
    qual_track = zeros(length(back_tracker), length(img_names));
    back_map = [zeros(1,length(img_names)-1);back_map];
    for i=1:length(img_names)
        try
            rmdir([exp_dir 'MySegmentTracks/' mask_type '/Length' num2str(min_length) '/' directories{i} '/'],'s');
        end
    end
    all_tracks = zeros(size(back_map,1), size(back_map,2) + 1);
    cur_tracking = [];
    new_trackids = [];
    counter = 0;
    all_len = zeros(length(img_names)-1,1);
    for i=length(img_names):-1:2
        % -1 to compensate the 0 added to back_map
        back_tracker = find(back_map(:,i-1)~=0) - 1;
        all_tracks(1:counter,i) = cur_tracking;
        new_tracks = setdiff(back_tracker, cur_tracking);
        all_tracks(counter+1:counter+length(new_tracks),i) = new_tracks;
        new_trackids = [new_trackids;new_tracks];
        all_len(i-1) = length(new_tracks);
        cur_tracking = [cur_tracking;new_tracks];
        cur_tracking = back_map(cur_tracking+1, i-1);
        counter = counter + length(new_tracks);
    end
    all_tracks(1:counter,1) = cur_tracking;
    keep_tracks = sum(all_tracks~=0,2) >= min_length;
    % new_trackids: first tracks are those ending in the last frame, then
    % tracks ending in the next-to-last frame, then previous frame, ...
    new_trackids = new_trackids(keep_tracks);
    k2 = keep_tracks(counter:-1:1);
    new_len = cumsum(k2);
    len = diff([0;new_len(cumsum(all_len))]);
    % Convert back to a cell array
    new_trackids = mat2cell(new_trackids,len(end:-1:1));
    new_trackids = new_trackids(end:-1:1);
    % Retain only tracks more than a certain length
    all_tracks = all_tracks(keep_tracks, :);
        
        
    for i=length(img_names):-1:1
        img_ext = get_img_extension([exp_dir '/JPEGImages/' directories{i} '/' img_names{i}]);
        I = imread([exp_dir '/JPEGImages/' directories{i} '/' img_names{i} img_ext]);
        var = load([exp_dir 'MySegmentsMat/' mask_type '/' directories{i} '/' img_names{i} '.mat']);
        masks = var.masks;
        if ~isempty(merge_plots)
            cmap = SvmSegm_labelcolormap(255);
            merged_img = I;
        end
            for j=1:length(all_tracks)
                track_dir = [exp_dir 'MySegmentTracks/' mask_type '/Length' num2str(min_length) '/' directories{i} '/' int2str(j) '/'];
                if ~exist(track_dir,'dir')
                    mkdir(track_dir);
                end
                if all_tracks(j,i) == 0
                    continue;
                end
                if isempty(merge_plots)
                    merged_img = merge_img(masks(:,:,all_tracks(j,i)), I);
                    cmap = [0 0 0;1 1 1];
                    imwrite(masks(:,:,all_tracks(j,i)), cmap, [track_dir num2str(i) '.png']);
                    imwrite(merged_img, [track_dir num2str(i) '.jpg']);
                else
                    if ismember(j, merge_plots)
                        merged_img = merge_img(masks(:,:,all_tracks(j,i)), merged_img, cmap(j+1,:),0.6);
                    end
                end
            end
            if ~isempty(merge_plots)
                track_dir = [exp_dir 'MySegmentTracks/' mask_type '/Length' num2str(min_length) '/' directories{i} '/Merged/'];
                if ~exist(track_dir,'dir')
                    mkdir(track_dir);
                end
                imwrite(merged_img, [track_dir num2str(i) '.jpg']);
            end
        end
    if nargout > 1
        num_tracks = size(all_tracks,1);
    end
end