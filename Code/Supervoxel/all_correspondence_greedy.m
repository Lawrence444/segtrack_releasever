function [M, clust_row, clust_col, Pixels] = all_correspondence_greedy(dataset, exp_dir, mask_type)
% REMEMBER TO ADD THESE PATHS!
%addpath(genpath('d:/git/csi'));
%addpath(genpath('d:/git/fuxin-library'));
    DefaultVal('mask_type','WithOpticalFlow');

    load([exp_dir 'intermediate_' dataset '.mat'], 'all_to_save');
    img_names = {all_to_save.img_name};
    directories = {all_to_save.directories};
    % First, compute all the superpixels
    sup_sizes = cell(length(img_names),1);
    sup_assignment = cell(length(img_names),1);
    scores = cell(length(img_names),1);
    bag_ids = cell(length(img_names),1);
    Pixels = cell(length(img_names),1);
    all_preds = cell(length(img_names),1);
    PB = cell(length(img_names),1);
    Centroid = cell(length(img_names),1);
    for i=1:length(img_names)
        var = load([exp_dir 'PB/' dataset '/' img_names{i} '_PB.mat']);
        PB{i} = var.gPb_thin;
        all_preds{i} = cell2mat(all_to_save(i).pred');
        masks_cur = load([exp_dir 'MySegmentsMat/' mask_type '/' directories{i} '/' img_names{i} '.mat']);
        masks_cur = masks_cur.masks;
        sc = max(all_preds{i},[],1);
        % The trackid for the first and second frames are the 
        % The first new_trackid is already at the end of the second frame
        % therefore we need to rearrange scores from 1 - 2
        if i > 1
            all_trackid = cell2mat(all_to_save(i-1).new_trackid);
        else
            all_trackid = 1:size(masks_cur,3);
        end
        masks_cur = masks_cur(:,:,all_trackid);
        [sup_sizes{i}, sup_assignment{i}, scores{i}, bag_ids{i}, Pixels{i}] = generate_superpixels_from_segresults(masks_cur, sc',[],0.2);
        % Re-arrange scores, because masks_cur has been re-arranged on
        % all_trackid, the scores also needs such re-arrangement
        all_preds{i} = all_preds{i}(cell2mat(all_to_save(i).new_trackid),bag_ids{i});
        Centroid{i} = superpix_regionprops(uint16(Pixels{i}));
        num_sup = size(Centroid{i},1);
        % Load image for color statistics
        img_ext = get_img_extension([exp_dir 'JPEGImages/' dataset '/'], img_names{i});
        I = imread([exp_dir 'JPEGImages/' dataset '/' img_names{i} img_ext]);
        % Assume color image
        I = reshape(I,size(I,1) * size(I,2), size(I,3));
        real_sup_size = histc(Pixels{i}(:),1:num_sup);
        R_sup = accumarray(Pixels{i}(:), I(:,1)) ./ real_sup_size;
        G_sup = accumarray(Pixels{i}(:), I(:,2)) ./ real_sup_size;
        B_sup = accumarray(Pixels{i}(:), I(:,3)) ./ real_sup_size;
        % Convert to LAB
        [L_sup, a_sup, b_sup] = rgb2lab(R_sup, G_sup, B_sup);
        sup_color{i} = [L_sup, a_sup, b_sup];
    end
    % Then do the optimization on each consecutive frame
    for i=1:length(img_names) - 1
        dist_mat =  pdist2(Centroid{i}, Centroid{i+1});
        cutoff_thres = norm(size(Pixels{i}),2) / sqrt(2);
% Only takes from dist_cutoff
        dist_cutoff = dist_mat < cutoff_thres * 0.1;
        lambda = 1;
        lambda2 = 0.02;
        [edgelet_sp1, accum_edge1] = compute_pairwise_info(Pixels{i}, PB{i}, 'Gb');
        structure_info.dist_cutoff = dist_cutoff';
        structure_info.edgelet_sp = edgelet_sp1;
        structure_info.accum_val = exp(-accum_edge1 * 255);
        structure_info.xdir_sups = double(bsxfun(@minus,Centroid{i+1}(:,1), Centroid{i}(:,1)'));
        structure_info.ydir_sups = double(bsxfun(@minus,Centroid{i+1}(:,2), Centroid{i}(:,2)'));
        structure_info.sup_size_prev = sup_sizes{i};
        structure_info.color_dist = pdist2(sup_color{i}, sup_color{i+1});
        M{i} = run_assignment_consecutive_frames(all_preds{i}(bag_ids{i+1},:),sup_assignment{i}, sup_sizes{i+1}, ...
               sup_assignment{i+1}, structure_info, lambda, lambda2);
       [clust_row{i}, clust_col{i}] = MySpectralCoClustering(M{i},0.5, dist_cutoff');
       if exist('DEBUG_FLAG') && DEBUG_FLAG
            Painted2 = PaintSuperpix(Pixels{i+1}, clust_row{i});
            Painted1 = PaintSuperpix(Pixels{i}, clust_col{i});
            figure,imshow(Painted1, cmap)
            figure,imshow(Painted2, cmap)
            PaintMotion(M{i}, structure_info.xdir_sups, structure_info.ydir_sups, Pixels{i})
       end
    end
end
