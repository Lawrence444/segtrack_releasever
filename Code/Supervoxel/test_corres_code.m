load build_corres.mat
load all_pix.mat
var1 = load('./PB/girl/5117-8_70161_PB.mat');
var2 = load('./PB/girl/5117-8_70162_PB.mat');
PB_1 = var1.gPb_thin;
PB_2 = var2.gPb_thin;
addpath(genpath('d:/git/csi'));
addpath(genpath('d:/git/fuxin-library'));
scores = max(pred{1});
scores2 = max(pred{1},[],2);
[sup_sizes, sup_assignment, scores, bag_ids, Pixels] = generate_superpixels_from_segresults(masks_cur, scores',[],0.2);
[sup_sizes2, sup_assignment2, scores2, bag_ids2, Pixels2] = generate_superpixels_from_segresults(masks_next, scores2,[],0.2);
save('all_pix.mat','Pixels','Pixels2');
Centroid = superpix_regionprops(uint16(Pixels));
Centroid2 = superpix_regionprops(uint16(Pixels2));
dist_mat =  pdist2(Centroid, Centroid2);
cutoff_thres = norm(size(Pixels),2) / sqrt(2);
% Only takes from dist_cutoff
dist_cutoff = dist_mat < cutoff_thres * 0.1;
lambda = 0.02;
lambda2 = 0.02;
[edgelet_sp1, accum_edge1] = compute_pairwise_info(Pixels, PB_1, 'Gb');
structure_info.dist_cutoff = dist_cutoff';
structure_info.edgelet_sp = edgelet_sp1;
structure_info.accum_val = exp(-accum_edge1 * 255);
structure_info.xdir_sups = double(bsxfun(@minus,Centroid2(:,1), Centroid(:,1)'));
structure_info.ydir_sups = double(bsxfun(@minus,Centroid2(:,2), Centroid(:,2)'));
structure_info.sup_size_prev = sup_sizes;
M = run_assignment_consecutive_frames(pred{1}(bag_ids2,bag_ids),sup_assignment, sup_sizes2, sup_assignment2, structure_info, lambda, lambda2);
[clust_row, clust_col] = MySpectralCoClustering(M,0.4, dist_cutoff');
Painted2 = PaintSuperpix(Pixels2, clust_row);
Painted1 = PaintSuperpix(Pixels, clust_col);
figure,imshow(Painted1, cmap)
figure,imshow(Painted2, cmap)
PaintMotion(M, structure_info.xdir_sups, structure_info.ydir_sups, Pixels)