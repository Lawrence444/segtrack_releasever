function PaintMotion(M, xdir_sups, ydir_sups, Pixels)
% First generate the flow vector, then paint motion use flow2color
    C_dist_x = sum(M .* xdir_sups,1)';
    C_dist_y = sum(M .* ydir_sups,1)';
    flow_vx = C_dist_x(Pixels);
    flow_vy = C_dist_y(Pixels);
    figure,imshow(flowToColor(cat(3,flow_vx,flow_vy)));
end