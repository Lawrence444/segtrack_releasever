function M = run_assignment_consecutive_frames(scores, sup_assignment_prev, sup_sizes, sup_assignment, structure_info, lambda, lambda2)
% Matrix-valued random variables the size of num_sups1 * num_sups2, cut off
% by dist_cutoff
    scores = double(scores);
    loss_func = @(x) consecutive_assignment_loss(x, sup_assignment_prev, sup_sizes, sup_assignment,scores, structure_info, lambda, lambda2);
    loss_func_real = @(x) consecutive_real_assignment_loss(x, sup_assignment_prev, sup_sizes, sup_assignment,scores, structure_info, lambda, lambda2);
    l1_proj_func = @(x) multiple_l1_projection(x, size(sup_assignment_prev,1));
        % Estimate the parameters
%    options.optTol = 1e-2;
%    options.verbose = 0;
%    M = minConf_SPG(loss_func, zeros(size(sup_assignment, 1), size(sup_assignment_prev,1)), l1_proj_func, options);    
    options.optTol = 1e-3;
    options.progTol = 1e-7;
    options.verbose = 2;
    options.maxIter = 4000;
%    options.corrections = 10;
%    options.bbinit = 1;
    M = minConf_SPG(loss_func_real, zeros(size(sup_assignment, 1) * size(sup_assignment_prev,1),1), l1_proj_func, options);    
    M = reshape(M, size(sup_assignment, 1) ,size(sup_assignment_prev,1));
end