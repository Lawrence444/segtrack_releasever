function Painted_Pixels = PaintSuperpix(Pixels, clust)
    Painted_Pixels = zeros(size(Pixels));
    for i=0:max(clust)
        Painted_Pixels(ismember(Pixels, find(clust==i))) = i;
    end
end